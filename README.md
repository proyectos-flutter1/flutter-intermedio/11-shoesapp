# Shoesapp
Este proyecto es del curso de Fernando Herrera

- [Fernando-Herrera.com](https://fernando-herrera.com)

Este es el diseño que hacemos en esta aplicación

[Curso de Flutter donde hacemos este diseño](https://fernando-herrera.com/#/curso/flutter-disenos)

[Canal de Youtube](https://www.youtube.com/channel/UCuaPTYj15JSkETGnEseaFFg?view_as=subscriber)

![Imagen final](https://fernando-herrera.com/youtube/shoesapp.png)

![Imagen final](https://fernando-herrera.com/youtube/shoesapp2.png)

# Version de Flutter

Flutter 1.22.4 • channel stable • https://github.com/flutter/flutter.git
Framework • revision 1aafb3a8b9 (6 months ago) • 2020-11-13 09:59:28 -0800
Engine • revision 2c956a31c0
Tools • Dart 2.10.4